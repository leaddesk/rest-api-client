# LeadDesk REST API PHP library

A small PHP helper library for using LeadDesk REST API. Library is based on Guzzle HTTP client <https://docs.guzzlephp.org/>.

See <https://leaddesk.com/docs/introduction-rest-api/> for more info on LeadDesk API.

**NOTICE!** This is a work-in-progress project at first, so breaking changes can happen until v1.0.0 is released.

## Install

The only officially supported way to use the library is to download it using composer <https://getcomposer.org/>.

```sh
composer require leaddesk/rest-api-client
```

## Usage

```php
# Create API client instance with yout API credentials
$client = new \LeadDesk\RestApiClient\Client(
  # Get your OAuth client_id and client_secret e.g. from LeadDesk Admin panel
  \LeadDesk\RestApiClient\ClientConfig::create('your_api_client_id', 'your_api_client_secret')
);

# Authenticate (=get access token) to your LeadDesk client
$client->authLeaddeskClientId($yourLeadDeskClientId)

# Fetch campaign with ID 1
$campaign = $client->campaigns()->get(1);
```

## How to contribute

TODO: Instructions to be written later when this project is more mature.
