<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient\Tests\Traits;

use GuzzleHttp;
use Psr;
use LeadDesk\RestApiClient;

/**
 * Adds support for faking and verifying guzzle interaction to testing class
 */
trait FakesGuzzle
{
    /**
     * Logged guzzle requests
     * @see http://docs.guzzlephp.org/en/stable/testing.html
     * @var array
     */
    protected $guzzleHistory;

    /**
     * Create fake guzzle client with mocked responses and request logging support
     *
     * @param array $requests simulated responses
     * @return GuzzleHttp\Client
     */
    public function fakeGuzzle(array $requests = [])
    {
        $this->guzzleHistory = [];
        $history = GuzzleHttp\Middleware::history($this->guzzleHistory);
        $mock = new GuzzleHttp\Handler\MockHandler($requests);
        $handlerStack = GuzzleHttp\HandlerStack::create($mock);
        $handlerStack->push($history);
        return new GuzzleHttp\Client(['handler' => $handlerStack]);
    }

    /**
     * Check that right amount of guzzle requests were sent
     *
     * @param int $expected how many requests were expected
     * @param string $message message shown on assert failure
     */
    public function assertRequestCount($expected, $message = '')
    {
        $this->assertCount($expected, $this->guzzleHistory, $message);
    }

    /**
     * Check that request had expected URI
     *
     * @param string $method
     * @param string $uri
     * @param string $message
     */
    public function assertUri($method, $uri)
    {
        $this->assertGreaterThan(0, count($this->guzzleHistory), 'Guzzle request history is empty');
        $request = $this->guzzleHistory[0]['request'];
        $this->assertEquals("{$method} {$uri}", "{$request->getMethod()} {$request->getUri()}", 'Request does not match');
    }

    /**
     * Return and remove next request from the guzzle request history
     *
     * @return object
     */
    public function nextJsonRequestContent()
    {
        $request = array_shift($this->guzzleHistory);
        $this->assertNotNull($request, 'Guzzle request history is empty');
        return json_decode($request['request']->getBody()->getContents());
    }

    /**
     * Return and remove next request from the guzzle request history
     *
     * @return object
     */
    public function nextRequest()
    {
        $request = array_shift($this->guzzleHistory);
        $this->assertNotNull($request, 'Guzzle request history is empty');
        return $request['request'];
    }

    /**
     * Create API client instance that is already authenticated
     *
     * @param array $responses mocked responses to return
     * @return RestApiClient\Client
     */
    protected function createAuthenticatedClient(array $responses = []): RestApiClient\Client
    {
        $client = $this->createClient(
            array_merge([$this->defaultAuthTokenResponse()], $responses)
        )->authLeaddeskClientId(1);
        $this->nextRequest(); // remove auth request from history
        return $client;
    }

    /**
     * Create API client instance
     *
     * @param array $responses mocked responses to return
     * @return RestApiClient\Client
     */
    protected function createClient(array $responses = []): RestApiClient\Client
    {
        return new RestApiClient\Client(
            RestApiClient\ClientConfig::create(self::CLIENT_ID, self::CLIENT_SECRET)
                ->withGuzzleClient($this->fakeGuzzle($responses))
        );
    }

    /**
     * Assert default authtoken content
     */
    protected function assertAuthToken(RestApiClient\AuthToken $token): void
    {
        $this->assertEquals(self::ACCESS_TOKEN, $token->accessToken, 'Access token mismatch');
        $this->assertEquals(self::REFRESH_TOKEN, $token->refreshToken, 'Refresh token mismatch');
        $this->assertEquals(self::TOKEN_TYPE, $token->tokenType, 'Token type mismatch');
        $this->assertGreaterThan(time(), $token->expiresAt, 'Token should not be expired');
    }

    /**
     * Create successful OAuth response
     *
     * @return Psr\Http\Message\ResponseInterface
     */
    public function defaultAuthTokenResponse(): Psr\Http\Message\ResponseInterface
    {
        return new GuzzleHttp\Psr7\Response(200, [], json_encode([
            'access_token' => self::ACCESS_TOKEN,
            'token_type' => self::TOKEN_TYPE,
            'expires_in' => self::EXPIRES_IN,
            'refresh_token' => self::REFRESH_TOKEN
        ]));
    }

    /**
     * Create JSON formatted error response
     *
     * @param array|object $body response content
     * @param int $status HTTP status code
     * @return Psr\Http\Message\ResponseInterface
     */
    public function createErrorResponse($body, int $status = 400): Psr\Http\Message\ResponseInterface
    {
        return $this->createJsonResponse($body, $status);
    }

    /**
     * Create empty response
     *
     * @param int $status HTTP status code
     * @return Psr\Http\Message\ResponseInterface
     */
    public function createEmptyResponse(int $status = 204): Psr\Http\Message\ResponseInterface
    {
        return new GuzzleHttp\Psr7\Response($status, [], '');
    }

    /**
     * Create JSON formatted response
     *
     * @param array|object $body response content
     * @param int $status HTTP status code
     * @return Psr\Http\Message\ResponseInterface
     */
    public function createJsonResponse($body, int $status = 200): Psr\Http\Message\ResponseInterface
    {
        return new GuzzleHttp\Psr7\Response($status, [], json_encode($body));
    }
}
