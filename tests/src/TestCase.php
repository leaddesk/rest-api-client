<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient\Tests;

use LeadDesk\RestApiClient;
use PHPUnit;

class TestCase extends PHPUnit\Framework\TestCase
{
    /**
     * Default OAuth credentials for tests
     */
    public const CLIENT_ID = 'a';
    public const CLIENT_SECRET = 'b';

    /**
     * Default OAuth access token response values
     */
    public const ACCESS_TOKEN = 'at';
    public const REFRESH_TOKEN = 'rt';
    public const EXPIRES_IN = 3600;
    public const TOKEN_TYPE = 'Bearer';
}
