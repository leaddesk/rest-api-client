<?php

declare(strict_types=1);

use LeadDesk\RestApiClient;

final class MalformedErrorExceptionTest extends RestApiClient\Tests\TestCase
{
    /**
     * Test exception properties access
     */
    public function testProperties(): void
    {
        $error = new RestApiClient\Exceptions\MalformedErrorException(499);
        $this->assertEquals(499, $error->status);
    }

    /**
     * Test accessing non-existing property
     */
    public function testInvalidAccess(): void
    {
        $this->expectException(UnexpectedValueException::class);
        $this->expectExceptionMessage('Access to undefined exception property `foobar`');
        $error = new RestApiClient\Exceptions\MalformedErrorException(499);
        $error->foobar;
    }
}
