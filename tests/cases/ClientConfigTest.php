<?php

declare(strict_types=1);

use LeadDesk\RestApiClient;

final class ClientConfigTest extends RestApiClient\Tests\TestCase
{
    /**
     * Test config object properties
     */
    public function testConfig(): void
    {
        $config = RestApiClient\ClientConfig::create('id', 'secret')
            ->withBaseUrl('https://foobar/');
        $this->assertEquals('id', $config->clientId);
        $this->assertEquals('secret', $config->clientSecret);
        $this->assertEquals('https://foobar/', $config->url);
        $this->assertInstanceOf(GuzzleHttp\Client::class, $config->client);
    }
}
