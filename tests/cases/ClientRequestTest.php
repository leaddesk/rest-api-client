<?php

declare(strict_types=1);

use LeadDesk\RestApiClient;

final class ClientRequestTest extends RestApiClient\Tests\TestCase
{
    use RestApiClient\Tests\Traits\FakesGuzzle;

    public static function providerMethods()
    {
        return [
            ['get'],
            ['post'],
            ['patch'],
            ['delete'],
        ];
    }

    /**
     * Test that HTTP errors are properly thrown as exceptions
     *
     * @dataProvider providerMethods
     */
    public function testJsonError(string $method): void
    {
        $this->expectException(RestApiClient\Exceptions\ErrorException::class);
        $this->expectExceptionMessage('[400] error: error_description');
        $this->createClient([
            $this->createErrorResponse(['error' => 'error', 'error_description' => 'error_description'], 400),
        ])->{$method}('error');
    }

    /**
     * Test that unexpected HTTP errors are properly handled
     *
     * @dataProvider providerMethods
     */
    public function testEmptyError(string $method): void
    {
        $this->expectException(RestApiClient\Exceptions\MalformedErrorException::class);
        $this->expectExceptionMessage('[500] Malformed error response from API');
        $this->createClient([
            $this->createEmptyResponse(500),
        ])->{$method}('error');
    }

    /**
     * Test that connection errors are properly handled
     *
     * @dataProvider providerMethods
     */
    public function testNetworkError(string $method): void
    {
        $this->expectException(RestApiClient\Exceptions\NetworkException::class);
        $this->expectExceptionMessage('Connection failure to LeadDesk Rest API');
        $this->createClient([
            new GuzzleHttp\Exception\ConnectException(
                'simulated network error',
                $this->createStub(Psr\Http\Message\RequestInterface::class)
            ),
        ])->{$method}('error');
    }

    /**
     * Test that unexpected HTTP errors are properly handled
     *
     * @dataProvider providerMethods
     */
    public function testRequestErrorWithoutResponse(string $method): void
    {
        $this->expectException(RestApiClient\Exceptions\NetworkException::class);
        $this->expectExceptionMessage('Connection failure to LeadDesk Rest API');
        $this->createClient([
            new GuzzleHttp\Exception\RequestException(
                'simulated network error',
                $this->createStub(Psr\Http\Message\RequestInterface::class)
            ),
        ])->{$method}('error');
    }
}
