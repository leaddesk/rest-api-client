<?php

declare(strict_types=1);

use LeadDesk\RestApiClient;

final class ClientAuthTest extends RestApiClient\Tests\TestCase
{
    use RestApiClient\Tests\Traits\FakesGuzzle;

    /**
     * Test that leaddesk_client_id authentication is properly done and stores authentication tokens
     */
    public function testAuthClientId(): void
    {
        $client = $this->createClient([
            $this->defaultAuthTokenResponse(),
        ])->authLeaddeskClientId(1);
        $this->assertRequestCount(1, 'Should have sent authentication request');
        $this->assertUri('POST', 'https://api.cloud.leaddesk.com/stable/oauth/access-token');
        $this->assertAuthToken($client->getAuthToken());
        $expectedRequest = (object)[
            'grant_type' => 'leaddesk_client_id',
            'client_id' => self::CLIENT_ID,
            'client_secret' => self::CLIENT_SECRET,
            'leaddesk_client_id' => 1,
        ];
        $this->assertEquals($expectedRequest, $this->nextJsonRequestContent(), 'Auth request mismatch');
    }

    /**
     * Test that password authentication is properly done and stores authentication tokens
     */
    public function testAuthPassword(): void
    {
        $client = $this->createClient([
            $this->defaultAuthTokenResponse(),
        ])->authPassword('user', 'pass');
        $this->assertRequestCount(1, 'Should have sent authentication request');
        $this->assertUri('POST', 'https://api.cloud.leaddesk.com/stable/oauth/access-token');
        $this->assertAuthToken($client->getAuthToken());
        $expectedRequest = (object)[
            'grant_type' => 'password',
            'client_id' => self::CLIENT_ID,
            'client_secret' => self::CLIENT_SECRET,
            'username' => 'user',
            'password' => 'pass',
        ];
        $this->assertEquals($expectedRequest, $this->nextJsonRequestContent(), 'Auth request mismatch');
    }

    /**
     * Test that client_user authentication is properly done and stores authentication tokens
     */
    public function testAuthClientUser(): void
    {
        $client = $this->createClient([
            $this->defaultAuthTokenResponse(),
        ])->authClientUser('user', 1);
        $this->assertRequestCount(1, 'Should have sent authentication request');
        $this->assertUri('POST', 'https://api.cloud.leaddesk.com/stable/oauth/access-token');
        $this->assertAuthToken($client->getAuthToken());
        $expectedRequest = (object)[
            'grant_type' => 'client_user',
            'client_id' => self::CLIENT_ID,
            'client_secret' => self::CLIENT_SECRET,
            'username' => 'user',
            'leaddesk_client_id' => 1,
        ];
        $this->assertEquals($expectedRequest, $this->nextJsonRequestContent(), 'Auth request mismatch');
    }

    /**
     * Test that authentication can handle wrong zone error response
     */
    public function testWrongZoneAuth(): void
    {
        $client = $this->createClient([
            $this->createErrorResponse([
                'error' => 'wrong_zone',
                'error_description' => 'Client is not in this zone. Please find ...',
                'url' => 'https://api.zone.leaddesk.com/stable/',
            ], 400),
            $this->defaultAuthTokenResponse(),
        ])->authPassword('user', 'pass');
        $this->assertRequestCount(2, 'Should have sent 2 authentication requests');
        $this->assertUri('POST', 'https://api.cloud.leaddesk.com/stable/oauth/access-token');
        $this->nextRequest();
        $this->assertUri('POST', 'https://api.zone.leaddesk.com/stable/oauth/access-token');
        $this->assertAuthToken($client->getAuthToken());
        $expectedRequest = (object)[
            'grant_type' => 'password',
            'client_id' => self::CLIENT_ID,
            'client_secret' => self::CLIENT_SECRET,
            'username' => 'user',
            'password' => 'pass',
        ];
        $this->assertEquals($expectedRequest, $this->nextJsonRequestContent(), 'Auth request mismatch');
    }

    /**
     * Test that authentication error throws proper exception
     */
    public function testAuthError(): void
    {
        $this->expectException(RestApiClient\Exceptions\ErrorException::class);
        $this->expectExceptionMessage('[401] invalid_credentials: The user credentials were incorrect.');
        $this->createClient([
            $this->createErrorResponse([
                'error' => 'invalid_credentials',
                'error_description' => 'The user credentials were incorrect.',
            ], 401),
        ])->authPassword('user', 'pass');
    }
}
