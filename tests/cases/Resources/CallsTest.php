<?php

declare(strict_types=1);

use LeadDesk\RestApiClient;

final class CallsTest extends RestApiClient\Tests\TestCase
{
    use RestApiClient\Tests\Traits\FakesGuzzle;

    /**
     * Test calls uploadRecording method
     */
    public function testUploadRecording(): void
    {
        $client = $this->createAuthenticatedClient([$this->createJsonResponse(['url' => 'some/url'])]);
        fopen("fake.mp3", "w");
        $this->assertEquals((object)['url' => 'some/url'], $client->calls()->uploadRecording(1, './fake.mp3'));
        $this->assertUri('PUT', 'https://api.cloud.leaddesk.com/stable/calls/1/record_file');
        unlink("fake.mp3");
    }
}
