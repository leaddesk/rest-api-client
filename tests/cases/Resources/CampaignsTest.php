<?php

declare(strict_types=1);

use LeadDesk\RestApiClient;

final class CampaignsTest extends RestApiClient\Tests\TestCase
{
    use RestApiClient\Tests\Traits\FakesGuzzle;

    /**
     * Test campaigns get method
     */
    public function testGet(): void
    {
        $client = $this->createAuthenticatedClient([$this->createJsonResponse(['id' => 1, 'name' => 'campaign'])]);
        $this->assertEquals(
            (object)['id' => 1, 'name' => 'campaign'],
            $client->campaigns()->get(1)
        );
        $this->assertUri('GET', 'https://api.cloud.leaddesk.com/stable/campaigns/1');
    }

    /**
     * Test campaigns create method
     */
    public function testPost(): void
    {
        $client = $this->createAuthenticatedClient([$this->createJsonResponse(['id' => 1])]);
        $this->assertEquals((object)['id' => 1], $client->campaigns()->create(['name' => 'foo']));
        $this->assertUri('POST', 'https://api.cloud.leaddesk.com/stable/campaigns');
        $this->assertEquals((object)['name' => 'foo'], $this->nextJsonRequestContent());
    }

    /**
     * Test campaigns create method
     */
    public function testUpdate(): void
    {
        $client = $this->createAuthenticatedClient([$this->createEmptyResponse()]);
        $this->assertNull($client->campaigns()->update(1, ['name' => 'foo']));
        $this->assertUri('PATCH', 'https://api.cloud.leaddesk.com/stable/campaigns/1');
        $this->assertEquals((object)['name' => 'foo'], $this->nextJsonRequestContent());
    }

    /**
     * Test campaigns delete method
     */
    public function testDelete(): void
    {
        $client = $this->createAuthenticatedClient([$this->createEmptyResponse()]);
        $this->assertNull($client->campaigns()->delete(1));
        $this->assertUri('DELETE', 'https://api.cloud.leaddesk.com/stable/campaigns/1');
    }

    /**
     * Test campaigns list method
     */
    public function testList(): void
    {
        $collectionResponse = (object)[
            'collection' => [
                (object)[
                    'id' => 1,
                    'name' => 'foo',
                ]
            ],
            '_links' => (object)[
                'prev' => null,
                'self' => '...',
                'next' => '...'
            ],
            '_meta' => (object)[
                'count' => 1
            ]
        ];
        $client = $this->createAuthenticatedClient([$this->createJsonResponse($collectionResponse)]);
        $this->assertEquals($collectionResponse, $client->campaigns()->list());
        $this->assertUri('GET', 'https://api.cloud.leaddesk.com/stable/campaigns');
    }
}
