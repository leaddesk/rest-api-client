<?php

declare(strict_types=1);

use LeadDesk\RestApiClient;

final class UsersTest extends RestApiClient\Tests\TestCase
{
    use RestApiClient\Tests\Traits\FakesGuzzle;

    /**
     * Test users logout method
     */
    public function testLogout(): void
    {
        $client = $this->createAuthenticatedClient([$this->createEmptyResponse()]);
        $this->assertNull($client->users()->logout(12345));
        $this->assertUri('POST', 'https://api.cloud.leaddesk.com/stable/users/12345/operations/logout');
    }

    /**
     * Test users list method
     */
    public function testList(): void
    {
        $collectionResponse = (object) [
            'collection' => [
                (object) [
                    'id' => 1,
                    'name' => 'foo',
                ],
                (object) [
                    'id' => 2,
                    'name' => 'bar',
                ],
            ],
            '_links' => (object) [
                'prev' => null,
                'self' => '...',
                'next' => '...',
            ],
            '_meta' => (object) [
                'count' => 2,
            ],
        ];

        $client = $this->createAuthenticatedClient([$this->createJsonResponse($collectionResponse)]);
        $this->assertEquals($collectionResponse, $client->users()->list());
        $this->assertUri('GET', 'https://api.cloud.leaddesk.com/stable/users');
    }

    /**
     * Test get user login token method
     */
    public function testGetUserLoginToken(): void
    {
        $response = (object) [
            'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJleHAiOjE1MzI5MzA4MTEsInVzZXIiOjEsIm1ldGEiOm51bGx9.JV4R5jLSyT0QFo6DEG37U53U5LUruw-87oMrg8vAJpyxEGrUWrvUu5vbLwz2qIP4xs5gBF0IC0gklCzHTO1cJQ',
            'expires' => '2018-07-30T08:00:00+03:00',
        ];

        $client = $this->createAuthenticatedClient([$this->createJsonResponse($response)]);
        $this->assertEquals($response, $client->users()->getUserLoginToken(2));
        $this->assertUri('GET', 'https://api.cloud.leaddesk.com/stable/users/2/login_token');
    }

    /**
     * Test get user login token method with additional meta information
     */
    public function testGetUserLoginTokenWithMeta(): void
    {
        $response = (object) [
            'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJleHAiOjE1MzI5MzA4MTEsInVzZXIiOjEsIm1ldGEiOm51bGx9.JV4R5jLSyT0QFo6DEG37U53U5LUruw-87oMrg8vAJpyxEGrUWrvUu5vbLwz2qIP4xs5gBF0IC0gklCzHTO1cJQ',
            'expires' => '2018-07-30T08:00:00+03:00',
        ];

        $client = $this->createAuthenticatedClient([$this->createJsonResponse($response)]);
        $requestParams = ['meta' => 'test@example.com'];
        $this->assertEquals($response, $client->users()->getUserLoginToken(2, $requestParams));
        $this->assertUri('GET', 'https://api.cloud.leaddesk.com/stable/users/2/login_token?' . http_build_query($requestParams));
    }

    /**
     * Test create user
     */
    public function testCreateUser(): void
    {
        $requestBody = [
            'name' => 'Foo bar name',
            'username' => 'foo_bar_username',
            'role' => 'agent',
        ];
        $response = (object) [
            'id' => 1,
        ];

        $client = $this->createAuthenticatedClient([$this->createJsonResponse($response)]);
        $this->assertEquals($response, $client->users()->create($requestBody));
        $this->assertUri('POST', 'https://api.cloud.leaddesk.com/stable/users');
        $this->assertEquals((object) $requestBody, $this->nextJsonRequestContent());
    }

    /**
     * Test copy user
     */
    public function testCopyUser(): void
    {
        $requestBody = [
            'name' => 'Foo bar name',
            'username' => 'foo_bar_username',
        ];
        $response = (object) [
            'id' => 2,
        ];

        $client = $this->createAuthenticatedClient([$this->createJsonResponse($response)]);
        $this->assertEquals($response, $client->users()->copy(1, $requestBody));
        $this->assertUri('POST', 'https://api.cloud.leaddesk.com/stable/users/1/operations/copy');
        $this->assertEquals((object) $requestBody, $this->nextJsonRequestContent());
    }
}
