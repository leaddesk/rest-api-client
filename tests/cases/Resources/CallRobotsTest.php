<?php

declare(strict_types=1);

use LeadDesk\RestApiClient;

final class CallRobotsTest extends RestApiClient\Tests\TestCase
{
    use RestApiClient\Tests\Traits\FakesGuzzle;

    /**
     * Test call robots get method
     */
    public function testGet(): void
    {
        $client = $this->createAuthenticatedClient([$this->createJsonResponse(['id' => 1234, 'name' => 'robot 1'])]);
        $this->assertEquals(
            (object)['id' => 1234, 'name' => 'robot 1'],
            $client->callRobots()->get(1)
        );
        $this->assertUri('GET', 'https://api.cloud.leaddesk.com/stable/call_robots/1');
    }

    /**
     * Test call robots create method
     */
    public function testPost(): void
    {
        $client = $this->createAuthenticatedClient([$this->createJsonResponse(['id' => 1])]);
        $this->assertEquals((object)['id' => 1], $client->callRobots()->create(['name' => 'test robot']));
        $this->assertUri('POST', 'https://api.cloud.leaddesk.com/stable/call_robots');
        $this->assertEquals((object)['name' => 'test robot'], $this->nextJsonRequestContent());
    }

    /**
     * Test call robots list method
     */
    public function testList(): void
    {
        $collectionResponse = (object)[
            'collection' => [
                (object)[
                    'id' => 1,
                    'name' => 'foo',
                ],
                (object)[
                    'id' => 2,
                    'name' => 'bar',
                ]
            ],
            '_links' => (object)[
                'prev' => null,
                'self' => '...',
                'next' => '...'
            ],
            '_meta' => (object)[
                'count' => 1
            ]
        ];
        $client = $this->createAuthenticatedClient([$this->createJsonResponse($collectionResponse)]);
        $this->assertEquals($collectionResponse, $client->callRobots()->list());
        $this->assertUri('GET', 'https://api.cloud.leaddesk.com/stable/call_robots');
    }

    /**
     * Test call robots start operation
     */
    public function testStart(): void
    {
        $client = $this->createAuthenticatedClient([$this->createEmptyResponse()]);
        $this->assertEquals(
            '',
            $client->callRobots()->start(1234)
        );
        $this->assertUri('POST', 'https://api.cloud.leaddesk.com/stable/call_robots/1234/operations/start');
    }

    /**
     * Test call robots stop operation
     */
    public function testStop(): void
    {
        $client = $this->createAuthenticatedClient([$this->createEmptyResponse()]);
        $this->assertEquals(
            '',
            $client->callRobots()->stop(1234)
        );
        $this->assertUri('POST', 'https://api.cloud.leaddesk.com/stable/call_robots/1234/operations/stop');
    }
}
