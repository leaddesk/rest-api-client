<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient;

use GuzzleHttp;
use Psr;

/**
 * LeadDesk Rest API Client
 */
class Client
{
    /**
     * Client configuration
     */
    private ClientConfig $config;

    /**
     * Access token after authentication
     */
    private ?AuthToken $authToken;

    /**
     * Create API client
     *
     * @param ClientConfig $config client configuration
     */
    public function __construct(ClientConfig $config)
    {
        $this->config = $config;
        $this->authToken = null;
    }

    /**
     * Authenticate using password grant
     *
     * @param string $username leaddesk username
     * @param string $password leaddesk password
     * @return self
     */
    public function authPassword(string $username, string $password): self
    {
        return $this->authRequest([
            'grant_type' => 'password',
            'client_id' => $this->config->clientId,
            'client_secret' => $this->config->clientSecret,
            'username' => $username,
            'password' => $password,
        ]);
    }

    /**
     * Authenticate using leaddesk_client_id grant
     *
     * @param int $leaddeskClientId leaddesk client id
     * @return self
     */
    public function authLeaddeskClientId(int $leaddeskClientId): self
    {
        return $this->authRequest([
            'grant_type' => 'leaddesk_client_id',
            'client_id' => $this->config->clientId,
            'client_secret' => $this->config->clientSecret,
            'leaddesk_client_id' => $leaddeskClientId,
        ]);
    }

    /**
     * Authenticate using client_user grant
     *
     * @param string $username leaddesk username
     * @param int $leaddeskClientId leaddesk client id
     * @return self
     */
    public function authClientUser(string $username, int $leaddeskClientId): self
    {
        return $this->authRequest([
            'grant_type' => 'client_user',
            'client_id' => $this->config->clientId,
            'client_secret' => $this->config->clientSecret,
            'username' => $username,
            'leaddesk_client_id' => $leaddeskClientId,
        ]);
    }

    /**
     * Send authentication request and store access token
     *
     * @param array $request auth request body
     * @return self
     */
    protected function authRequest(array $request): self
    {
        try {
            $this->saveAuth(
                $this->requestRaw('POST', 'oauth/access-token', ['json' => $request])
            );
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            if ($e->hasResponse()) {
                $body = json_decode((string)($e->getResponse()->getBody()));
                // If we authenticated to wrong Rest API endpoint, try again with correct URL
                if ($body->error === 'wrong_zone' && $body->url) {
                    $this->config->url = $body->url;
                    $this->saveAuth(
                        $this->request('POST', 'oauth/access-token', ['json' => $request])
                    );
                    return $this;
                }
            }
            throw $this->createExceptionFromResponse($e);
        }
        return $this;
    }

    /**
     * Save authentication token response to be used in next requests
     *
     * @param Psr\Http\Message\ResponseInterface $response OAuth endpoint response
     */
    protected function saveAuth(Psr\Http\Message\ResponseInterface $response)
    {
        $this->authToken = AuthToken::createFromResponse($response);
    }

    /**
     * Send request to API and convert request exceptions to client exceptions
     *
     * @param string $method HTTP method
     * @param string $path API URL path
     * @param array $options Guzzle request options
     */
    protected function request(string $method, string $path, array $options): Psr\Http\Message\ResponseInterface
    {
        try {
            return $this->requestRaw($method, $path, $options);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            throw $this->createExceptionFromResponse($e);
        } catch (\GuzzleHttp\Exception\TransferException $e) {
            throw new Exceptions\NetworkException($e);
        }
    }

    /**
     * Send request to API without catching any exceptions
     *
     * @param string $method HTTP method
     * @param string $path API URL path
     * @param array $options Guzzle request options
     */
    protected function requestRaw(string $method, string $path, array $options = []): Psr\Http\Message\ResponseInterface
    {
        if ($this->authToken) {
            $options['headers'] = $options['headers'] ?? [];
            $options['headers']['Authorization'] = $this->authToken->authorizationHeader();
        }
        return $this->config->client->request($method, $this->config->url . $path, $options);
    }

    /**
     * Create client library exception based on Guzzle exception
     *
     * @param \GuzzleHttp\Exception\RequestException $exception
     * @return Exceptions\ClientException
     */
    protected function createExceptionFromResponse(\GuzzleHttp\Exception\RequestException $exception): Exceptions\ClientException
    {
        if ($exception->hasResponse()) {
            $response = $exception->getResponse();
            $body = (string)$response->getBody();
            $error = json_decode($body);
            if ($error === null) {
                return new Exceptions\MalformedErrorException($response->getStatusCode());
            }

            if ($response->getStatusCode() === 422) {
                return new Exceptions\ValidationException($error->error, $error->error_description, $response->getStatusCode(), $error->error_validation);
            }

            return new Exceptions\ErrorException($error->error, $error->error_description, $response->getStatusCode());
        }
        return new Exceptions\NetworkException($exception);
    }

    /**
     * Access token instance if already authenticated
     *
     * @return AuthToken|null
     */
    public function getAuthToken(): ?AuthToken
    {
        return $this->authToken;
    }

    /**
     * Send GET request to API
     *
     * @param string $path API resource path
     * @param array $query query parameters
     * @return array|\stdClass response
     */
    public function get(string $path, array $query = [])
    {
        return json_decode((string)$this->request(
            'GET',
            $path,
            [
                "query" => $query
            ]
        )->getBody());
    }

    /**
     * Send PATCH request to API
     *
     * @param string $path API resource path
     * @param array $body request body which gets converted into json
     * @return array|\stdClass response
     */
    public function patch(string $path, array $body = [])
    {
        return json_decode((string)$this->request(
            'PATCH',
            $path,
            [
                "json" => $body
            ]
        )->getBody());
    }

    /**
     * Send GET request to API
     *
     * @param string $path API resource path
     * @param array $query query parameters
     * @return array|\stdClass response
     */
    public function delete(string $path, array $query = [])
    {
        return json_decode((string)$this->request(
            'DELETE',
            $path,
            [
                "query" => $query
            ]
        )->getBody());
    }

    /**
     * Send PATCH request to API
     *
     * @param string $path API resource path
     * @param array $body request body which gets converted into json
     * @return array|\stdClass response
     */
    public function post(string $path, array $body = [])
    {
        return json_decode((string)$this->request(
            'POST',
            $path,
            [
                "json" => $body
            ]
        )->getBody());
    }

    /**
     * Send PUT request to API
     *
     * @param string $path API resource path
     * @param array $body request body
     * @return array|\stdClass response
     */
    public function put(string $path, array $body = [])
    {
        return json_decode((string)$this->request(
            'PUT',
            $path,
            $body
        )->getBody());
    }

    /**
     * API methods for /calls resource
     *
     * @return Resources\Calls
     */
    public function calls(): Resources\Calls
    {
        return new Resources\Calls($this);
    }

    /**
     * API methods for /campaigns resource
     *
     * @return Resources\Campaigns
     */
    public function campaigns(): Resources\Campaigns
    {
        return new Resources\Campaigns($this);
    }

    /**
     * API methods for /call_robots resource
     *
     * @return Resources\CallRobots
     */
    public function callRobots(): Resources\CallRobots
    {
        return new Resources\CallRobots($this);
    }

    /**
     * API methods for /notifications resource
     *
     * @return Resources\Notifications
     */
    public function notifications(): Resources\Notifications
    {
        return new Resources\Notifications($this);
    }

    /**
     * API methods for /users resource
     *
     * @return Resources\Users
     */
    public function users(): Resources\Users
    {
        return new Resources\Users($this);
    }
}
