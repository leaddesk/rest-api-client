<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient;

/**
 * Rest API access token response content
 */
class AuthToken
{
    public string $accessToken;
    public string $tokenType;
    public string $refreshToken;
    public int $expiresAt;

    /**
     * Create from access token response
     *
     * @param \Psr\Http\Message\ResponseInterface $jsonResponse
     * @return AuthToken
     */
    public static function createFromResponse(\Psr\Http\Message\ResponseInterface $jsonResponse): AuthToken
    {
        $decoded = json_decode((string)$jsonResponse->getBody());
        $tokens = new static();
        $tokens->accessToken = $decoded->access_token;
        $tokens->tokenType = $decoded->token_type;
        $tokens->refreshToken = $decoded->refresh_token;
        $tokens->expiresAt = time() + $decoded->expires_in;
        return $tokens;
    }

    /**
     * Return value for Authorization header
     *
     * @return string
     */
    public function authorizationHeader(): string
    {
        return "Bearer {$this->accessToken}";
    }
}
