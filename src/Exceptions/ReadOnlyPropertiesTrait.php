<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient\Exceptions;

use UnexpectedValueException;

/**
 * API client base exception
 */
trait ReadOnlyPropertiesTrait
{
    /**
     * Error data
     */
    protected array $data;

    /**
     * Access magic properties
     *
     * @param string $key
     * @return mixed
     */
    public function __get(string $key)
    {
        if (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        }

        throw new UnexpectedValueException("Access to undefined exception property `{$key}`");
    }
}
