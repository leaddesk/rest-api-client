<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient\Exceptions;

/**
 * Exception thrown when API request gives an error response
 *
 * @property-read string $error Logical error from API
 * @property-read string $description Human readable error message from API
 * @property-read int $status API error HTTP status code
 */
class ErrorException extends ClientException
{
    use ReadOnlyPropertiesTrait;

    /**
     * @param string $error logical error string
     * @param string $description human readdable error text in English
     * @param int $status HTTP status code
     */
    public function __construct(string $error, string $description, int $status)
    {
        parent::__construct("[{$status}] {$error}: {$description}", $status);
        $this->data = [
            'error' => $error,
            'description' => $description,
            'status' => $status,
        ];
    }
}
