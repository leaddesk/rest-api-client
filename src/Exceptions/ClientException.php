<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient\Exceptions;

/**
 * API client base exception
 */
class ClientException extends \Exception
{
}
