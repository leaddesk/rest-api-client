<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient\Exceptions;

use UnexpectedValueException;

/**
 * Exception thrown when API request gives an malformed error response
 *
 * @property-read int $status API error HTTP status code
 */
class MalformedErrorException extends ClientException
{
    use ReadOnlyPropertiesTrait;

    /**
     * @param int $status HTTP status code
     */
    public function __construct(int $status)
    {
        parent::__construct("[{$status}] Malformed error response from API", $status);
        $this->data = [
            'status' => $status,
        ];
    }
}
