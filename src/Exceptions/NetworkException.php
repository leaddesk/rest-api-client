<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient\Exceptions;

/**
 * Exception thrown when communication to LeadDesk Rest API is completely disrupted
 */
class NetworkException extends ClientException
{
    public function __construct(\Exception $parent)
    {
        parent::__construct("Connection failure to LeadDesk Rest API", 0, $parent);
    }
}
