<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient\Resources;

/**
 * API resource at /campaigns
 */
class Campaigns extends ResourceBase
{
    /**
     * List all campaigns
     *
     * @param array $params Request's URL parameters
     * @return \stdClass
     */
    public function list(array $params = [])
    {
        return $this->client->get("campaigns", $params);
    }

    /**
     * Read a single campaign
     *
     * @param int $campaignId
     * @return \stdClass
     */
    public function get(int $campaignId)
    {
        return $this->client->get("campaigns/{$campaignId}");
    }

    /**
     * Update/modify a single campaign
     *
     * @param int $campaignId
     * @param arrray $changes campaign properties to update/change
     * @return \stdClass|null
     */
    public function update(int $campaignId, array $changes)
    {
        return $this->client->patch("campaigns/{$campaignId}", $changes);
    }

    /**
     * Delete a single campaign
     *
     * @param int $campaignId
     * @return \stdClass|null
     */
    public function delete(int $campaignId)
    {
        return $this->client->delete("campaigns/{$campaignId}");
    }

    /**
     * Create a new campaign
     *
     * @param array $campaign campaign properties
     * @return \stdClass
     */
    public function create(array $campaign)
    {
        return $this->client->post("campaigns", $campaign);
    }
}
