<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient\Resources;

/**
 * API resource at /users
 */
class Users extends ResourceBase
{
    /**
     * Logout an user
     *
     * @param int $userId user to be logged out
     * @return \stdClass
     */
    public function logout(int $userId)
    {
        return $this->client->post("users/{$userId}/operations/logout");
    }

    /**
     * List all users
     *
     * @param array $params Request's URL parameters
     * @return \stdClass
     */
    public function list(array $params = [])
    {
        return $this->client->get("users", $params);
    }

    /**
     * Get user login token
     *
     * @param int $userId The user for whom the login token is to be retrieved
     * @param array $params Request's URL parameters
     * @return \stdClass
     */
    public function getUserLoginToken(int $userId, array $params = [])
    {
        return $this->client->get("users/{$userId}/login_token", $params);
    }

    /**
     * Create a user
     *
     * @param array $properties User properties
     */
    public function create(array $properties)
    {
        return $this->client->post("users", $properties);
    }

    /**
     * Copy a user
     *
     * @param int $userId The user from whom the copy is to be made
     * @param array $properties New user properties
     */
    public function copy(int $userId, array $properties)
    {
        return $this->client->post("users/{$userId}/operations/copy", $properties);
    }
}
