<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient\Resources;

use LeadDesk\RestApiClient\Client;

/**
 * Base class for API resources
 */
class ResourceBase
{
    protected Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }
}
