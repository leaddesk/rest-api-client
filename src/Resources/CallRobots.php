<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient\Resources;

/**
 * API resource at /call_robots
 */
class CallRobots extends ResourceBase
{
    /**
     * List all call robots
     *
     * @param array $params Request's URL parameters
     * @return \stdClass
     */
    public function list(array $params = [])
    {
        return $this->client->get("call_robots", $params);
    }

    /**
     * Read a single call robot
     *
     * @param int $callRobotId
     * @return \stdClass
     */
    public function get(int $callRobotId)
    {
        return $this->client->get("call_robots/{$callRobotId}");
    }

    /**
     * Create a new call robot
     *
     * @param array $callRobot call robot properties
     * @return \stdClass
     */
    public function create(array $callRobot)
    {
        return $this->client->post("call_robots", $callRobot);
    }

    /**
     * Update/modify a single call robot
     *
     * @param int $robotId
     * @param arrray $changes robot properties to update/change
     * @return \stdClass|null
     */
    public function update(int $callRobotId, array $changes)
    {
        return $this->client->patch("call_robots/{$callRobotId}", $changes);
    }

    /**
     * Start specified call robot
     *
     * @param int $callRobotId
     * @return \stdClass
     */
    public function start(int $callRobotId)
    {
        return $this->client->post("call_robots/{$callRobotId}/operations/start");
    }

    /**
     * Stop specified call robot
     *
     * @param int $callRobotId
     * @return \stdClass
     */
    public function stop(int $callRobotId)
    {
        return $this->client->post("call_robots/{$callRobotId}/operations/stop");
    }
}
