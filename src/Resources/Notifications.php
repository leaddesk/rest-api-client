<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient\Resources;

/**
 * API resource at /notifications
 */
class Notifications extends ResourceBase
{
    /**
     * Send a notification to agent(s)
     *
     * @param array $params Request's URL parameters
     * @return \stdClass
     */
    public function sendToAgents(array $params = [])
    {
        return $this->client->post("notifications/agents", $params);
    }
}
