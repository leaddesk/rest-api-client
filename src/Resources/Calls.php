<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient\Resources;

/**
 * API resource at /calls
 */
class Calls extends ResourceBase
{
    /**
     * Upload recording
     *
     * @param int $callId
     * @param string $filePath
     * @return \stdClass
     */
    public function uploadRecording($callId, $filePath)
    {
        return $this->client->put("calls/{$callId}/record_file", ['body' => fopen($filePath, 'r')]);
    }
}
