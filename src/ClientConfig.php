<?php

declare(strict_types=1);

namespace LeadDesk\RestApiClient;

use GuzzleHttp;

/**
 * Rest API client configuration and settings
 */
class ClientConfig
{
    /**
     * Default LeadDesk Rest API base URL
     */
    public const DEFAULT_BASE_URL = 'https://api.cloud.leaddesk.com/stable/';

    /**
     * OAuth Client ID
     */
    public string $clientId;

    /**
     * OAuth Client Secret
     */
    public string $clientSecret;

    /**
     * LeadDesk Rest API base URL
     */
    public string $url;

    /**
     * Http client instance
     */
    public GuzzleHttp\Client $client;

    /**
     * Create API config
     *
     * @param string $clientId OAuth client ID
     * @param string $clientSecret OAuth client secret
     * @return ClientConfig
     */
    public static function create(string $clientId, string $clientSecret): ClientConfig
    {
        $config = new static();
        $config->clientId = $clientId;
        $config->clientSecret = $clientSecret;
        $config->url = self::DEFAULT_BASE_URL;
        $config->client = new GuzzleHttp\Client();
        return $config;
    }

    /**
     * Change API base URL
     *
     * @param string $url
     * @return self
     */
    public function withBaseUrl(string $url): self
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Define API HTTP client instance
     *
     * @param GuzzleHttp\Client $client
     * @return self
     */
    public function withGuzzleClient(GuzzleHttp\Client $client): self
    {
        $this->client = $client;
        return $this;
    }
}
